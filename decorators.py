from functools import wraps
from flask import g, url_for, redirect, flash

__author__ = 'isturunt'


def admin_required(f):
    """
    Requires user to be logged in as an admin.

    :param f: function to decorate
    :return: wrapper
    """
    @wraps(f)
    def wrapper(*args, **kwargs):
        if g.user is None or not g.user.is_admin():
            flash('You\'re not allowed to view the requested page (admin required).', category='error')
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return wrapper