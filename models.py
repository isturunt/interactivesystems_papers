from flask.ext.login import UserMixin
from InteractiveEdu_Papers import db, lm
from hashlib import md5

__author__ = 'isturunt'

ROLE_USER, ROLE_ADMIN = range(2)


@lm.user_loader
def get_user(ident):
    return User.query.get(int(ident))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    posts = db.relationship('Post', backref='user', lazy='dynamic')

    def get_avatar(self, size=80):
        return 'http://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + '?d=mm&s=' + str(size)

    def get_role(self):
        if self.role is ROLE_ADMIN:
            return 'admin'
        return 'user'

    def is_admin(self):
        return self.role is ROLE_ADMIN

    def __repr__(self):
        return "<User {nickname}:{role}>".format(
            nickname=self.nickname,
            role=self.get_role()
        )


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(300))
    authors = db.Column(db.String(300))
    overview = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)
    url = db.Column(db.String())
    ed = db.Column(db.String(300))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return "<Post {title}>".format(title=self.title[:40])