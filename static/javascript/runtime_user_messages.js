/**
 * Created by ilyaturuntaev on 06.01.15.
 */

var user_message = function(message, category){
    var that = $('<div class=alert></div>');
    var msg = message || '';
    that.html('<button type="button" class="close" data-dismiss="alert">&times;</button>&nbsp;' + msg);
    if(category == 'error'){
        that.addClass('alert-danger');
    }
    else if(category == 'warning'){
        that.addClass('alert-warning');
    }
    else if(category == 'info') {
        that.addClass('alert-info')
    }
    else if(category == 'success') {
        that.addClass('alert-success')
    }
    else {
        that.addClass('alert-info')
    }

    return that;
};