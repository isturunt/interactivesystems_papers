import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED = True

SECRET_KEY = 'y:Nrl}vnice0cp?[dsoupEt9mFwIh'

OPENID_PROVIDERS = [
    {
        'name': 'Google',
        'url': 'https://www.google.com/accounts/o8/id',
        'requires_username': False
    },
    {
        'name': 'Yahoo',
        'url': 'http://yahoo.com',
        'requires_username': False
    },
    {
        'name': 'Yandex',
        'url': 'http://openid.yandex.ru/<username>',
        'requires_username': True
    },
    {
        'name': 'AOL',
        'url': 'http://openid.aol.com/<username>',
        'requires_username': True
    },
    {
        'name': 'Flickr',
        'url': 'http://www.flickr.com/<username>',
        'requires_username': True
    },
]

DATABASE_NAME = 'InteractiveEduPapers.sqlite'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, DATABASE_NAME)
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASEDIR, 'db_repository')