import datetime
from flask import render_template, redirect, flash, g, url_for, session, request, abort, jsonify
from flask.ext.login import login_user, current_user, login_required, logout_user
from InteractiveEdu_Papers import app, oid, db
from decorators import admin_required
from forms import OpenIDLoginForm, PostForm
from models import User, ROLE_USER, Post

__author__ = 'isturunt'


@app.before_request
def add_user_to_g():
    g.user = current_user


@app.route('/')
@app.route('/index')
def index():
    posts = [{
        'timestamp': '01.01.2014',
        'title': 'Test Post 1',
        'authors': ', '.join(['Shupkin S.V.']),
        'author': User.query.first()
    }, {
        'timestamp': '01.01.2014',
        'title': 'Test Post 2',
        'authors': ', '.join(['Kochevnik D.U.']),
        'author': User.query.first()
    }
    ]
    posts = Post.query.all()
    return render_template('index.html', posts=posts)


@app.route('/login', methods=['GET', 'POST'])
@oid.loginhandler
def login():
    print "Well. Trying to login."
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    login_form = OpenIDLoginForm()
    if login_form.validate_on_submit():
        session['remember_me'] = login_form.remember_me.data
        return oid.try_login(login_form.openid.data, ask_for=['nickname', 'email'])
    return render_template(
        'login.html',
        form=login_form,
        providers=app.config['OPENID_PROVIDERS']
    )


@oid.errorhandler
def on_error(message):
    flash(message, category='error')


@oid.after_login
def after_login(resp):
    print "In after_login"
    if not resp.email:
        flash("Invalid login. Please try again", category='error')
        return redirect(url_for('index'))
    user = User.query.filter_by(email=resp.email).first()
    if not user:
        nickname = resp.nickname
        if not nickname:
            nickname = resp.email.split('@')[0]
        count = 1
        while User.query.filter_by(nickname=nickname).all():
            nickname += str(count)
            count += 1
        user = User(nickname=nickname, email=resp.email, role=ROLE_USER)
        db.session.add(user)
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember=remember_me)
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    flash('Logged out', category='info')
    return redirect(url_for('index'))


@app.route('/posts/<post_id>')
def show_post(post_id):
    post = Post.query.get(post_id)
    if not post:
        abort(404)
    return render_template('posts/show.html', post=post)


@app.route('/posts/new', methods=['GET', 'POST'])
@admin_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(
            title=form.title.data,
            authors=form.authors.data,
            overview=form.overview.data,
            url=form.url.data,
            ed=form.ed.data,
            user=g.user,
            timestamp=datetime.datetime.now()
        )
        db.session.add(post)
        db.session.commit()
        flash(message='Post successfully created', category='success')
        return redirect(url_for('index'))
    return render_template('posts/crud.html', form=form, form_title='New Post', submit_text='Create')


@app.route('/posts/<post_id>/edit', methods=['GET', 'POST'])
@admin_required
def edit_post(post_id):
    post = Post.query.get(post_id)
    if not post:
        abort(404)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.authors = form.authors.data
        post.overview = form.overview.data
        post.url = form.url.data
        post.ed = form.ed.data
        db.session.add(post)
        db.session.commit()
        flash('Post #{post_id} successfully updated'.format(post_id=post_id), category='success')
        return redirect(url_for('show_post', post_id=post_id))
    form.title.data = post.title
    form.authors.data = post.authors
    form.overview.data = post.overview
    form.url.data = post.url
    form.ed.data = post.ed
    return render_template('posts/crud.html', form=form, form_title='Edit Post #' + str(post_id), submit_text='Apply')


@app.route('/posts/_delete_post/<post_id>')
@admin_required
def delete_post(post_id):
    post = Post.query.get(post_id)
    db.session.delete(post)
    db.session.commit()
    return jsonify(result='Post #' + post_id + ' successfully deleted', deleted_post_id=post_id)
# https://isturunt@bitbucket.org/isturunt/interactivesystems_papers.git