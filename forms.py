from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField
from wtforms.validators import DataRequired

__author__ = 'isturunt'


class OpenIDLoginForm(Form):
    openid = StringField('openid', validators=[DataRequired(message='OpenID is required.')])
    remember_me = BooleanField('remember_me', default=False)


class PostForm(Form):
    title = StringField('title', validators=[DataRequired()])
    authors = StringField('authors', validators=[DataRequired()])
    url = StringField('url')
    ed = StringField('ed')
    overview = TextAreaField('overview')